import {listarItemsPorId} from './listaItems.js'

function getItemsDoMenuHorizontal(url) {
    try {
        fetch(url).then(function (response) {
            response.json().then(function (data) {
                setItemsDoMenuHorizontal(data.items)
            })
        })

    } catch (error) {
        console.log(error);
    }
}

function createItem(itemParams) {

    let section = document.querySelector('#itemMenuHorizontal');

    let sectionTemp = document.createElement('section');
    sectionTemp.setAttribute('id', itemParams['id'])
    sectionTemp.style.cursor = "pointer";

    let span = document.createElement('span');
    span.innerHTML = itemParams['name'];
    
    span.onclick = function load () {
        let sectionPrincipal = document.querySelector('#conteudo-principal');
        let sectionLocalAtual= document.querySelector('#local-atual');
        sectionLocalAtual.innerHTML = "<span> Página Principal </span> >  <span style='color:red;' >" + itemParams['name'] + "</span>"
        sectionPrincipal.style.display = 'none'

        listarItemsPorId(itemParams['id']);
        
    }

    sectionTemp.appendChild(span);

    section.appendChild(sectionTemp)

}

function setItemsDoMenuHorizontal(data) {
    data.map((itemParams) => {
        createItem(itemParams)
    })
}

getItemsDoMenuHorizontal('http://localhost:8888/api/V1/categories/list');

export {listarItemsPorId}
