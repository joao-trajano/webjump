function listarItemsPorId(id) {

    try {
        fetch('http://localhost:8888/api/V1/categories/' + id)
            .then(function (response) {
                response.json().then(function (data) {

                    let sectionConteudoPrincipal = document.querySelector('#conteudo-catalogo');
                    sectionConteudoPrincipal.innerHTML = '';

                    data.items.map((item) => {
                        sectionConteudoPrincipal.setAttribute("class", id);
                        // Criando a imagem do item
                            let img = document.createElement('img');

                            img.setAttribute('src', item['image'])
                            img.setAttribute('width', '100px');
                            img.setAttribute('height', '100px');
                        
                            let sectionImg = document.createElement('section');

                            sectionImg.style.border = "1px solid lightgrey";
                            sectionImg.style.padding = "4px";
                            sectionImg.style.display = "flex";
                            sectionImg.style.justifyContent = "center";
                            sectionImg.style.alignItems = "center";

                            sectionImg.appendChild(img)
                        // ------------------------------------------

                        // Container 
                            let sectionContainer = document.createElement('section');
                            sectionContainer.setAttribute("id", item['id']);
                            sectionContainer.setAttribute("class", id);
                            sectionContainer.style.display = "flex";
                            sectionContainer.style.flexDirection = "column";

                            let spanName = document.createElement('span');
                            let spanSectionName = document.createElement('section');
                            spanSectionName.style.textAlign = "center";
                            spanName.style.fontSize = "12px";
                            spanName.style.textAlign = "center";
                            
                            spanName.style.fontWeight = "bold";
                            spanName.innerHTML = item['name'];

                            let spanPrice = document.createElement('span');
                            let spanSectionPrice = document.createElement('section');
                            spanSectionPrice.style.textAlign = "center";
                            spanPrice.style.fontSize = "12px";
                            spanPrice.style.textAlign = "center";
                            
                            spanPrice.style.color = "#cc0000";
                            spanPrice.innerHTML =  "R$" + item['price'];
                            spanPrice.style.fontWeight = "bold";

                            let button = document.createElement('button');
                            button.innerHTML = "COMPRAR";
                            button.style.fontSize = "12px";
                            button.style.textAlign = "center";
                            button.style.color = "white";
                            button.style.fontWeight = "bold";
                            button.style.backgroundColor = "#48D1CC";
                            button.style.borderRadius = "5px";
                            button.style.border = "none";
                            button.style.paddingTop = "4px";
                            button.style.paddingBottom = "4px";
                            
                        // ------------------------------------------   

                        sectionContainer.appendChild(sectionImg);
                        spanSectionName.appendChild(spanName);
                        spanSectionPrice.appendChild(spanPrice);
                        sectionContainer.appendChild(spanSectionName);
                        sectionContainer.appendChild(spanSectionPrice);
                        sectionContainer.appendChild(button);
                        sectionConteudoPrincipal.appendChild(sectionContainer);
                    })

                })

            })

    } catch (error) {
        console.log(error);
    }
}

export { listarItemsPorId };
